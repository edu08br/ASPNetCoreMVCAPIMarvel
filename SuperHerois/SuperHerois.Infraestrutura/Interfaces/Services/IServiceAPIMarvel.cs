﻿using SuperHerois.Infraestrutura.Interfaces.Entity;
using System.Collections.Generic;

namespace SuperHerois.Infraestrutura.Services
{
    public interface IServiceAPIMarvel
    {
        List<IPersonagem> ListarTodos();

        IPersonagem ObterPersonagem(int IdPersonagem);

        IComics ObterComics(int IdComics);
    }
}