﻿using System;
using System.Collections.Generic;

namespace SuperHerois.Infraestrutura.Interfaces.Entity
{
    public interface IPersonagem
    {
        Int64 IdPersonagem { get; set; }
        string Nome { get; set; }
        string Descricao { get; set; }
        String UrlImagemPersonagem { get; set; }
        IList<IComics> ListaComics { get; set; }
    }
}