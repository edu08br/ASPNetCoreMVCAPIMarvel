﻿using System;

namespace SuperHerois.Infraestrutura.Interfaces.Entity
{
    public interface IComics
    {
        Int64 IdComic { get; set; }
        String URIRecurso { get; set; }
        String Nome { get; set; }
        String Descricao { get; set; }
        String UrlImagemCapa { get; set; }
    }
}