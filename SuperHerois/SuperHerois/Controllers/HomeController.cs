﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SuperHerois.Infraestrutura.Interfaces.Entity;
using SuperHerois.Infraestrutura.Services;
using System.Collections.Generic;

namespace SuperHerois.Controllers
{
    public class HomeController : Controller
    {
        private IServiceAPIMarvel _serviceApiMarvel;

        public HomeController(IServiceAPIMarvel serviceApiMarvel)
        {
            this._serviceApiMarvel = serviceApiMarvel;
        }

        public IActionResult Index()
        {
            IList<IPersonagem> listaPersonagem = _serviceApiMarvel.ListarTodos();

            ViewBag.ListaPersonagem = new SelectList(listaPersonagem, "IdPersonagem", "Nome");

            return View();
        }

        public IActionResult GetPersonagem(int IdPersonagem)
        {
            IPersonagem personagem = _serviceApiMarvel.ObterPersonagem(IdPersonagem);

            return Json(personagem);
        }

        public IActionResult GetComics(int IdComics)
        {
            IComics comics = _serviceApiMarvel.ObterComics(IdComics);

            return Json(comics);
        }
    }
}