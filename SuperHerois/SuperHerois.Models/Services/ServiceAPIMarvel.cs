﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SuperHerois.Infraestrutura.Interfaces.Entity;
using SuperHerois.Infraestrutura.Services;
using SuperHerois.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;

namespace SuperHerois.Models.Interfaces.Services
{
    public class ServiceAPIMarvel : IServiceAPIMarvel
    {
        #region VariaveisDaClasse

        private IServiceProvider ServiceProvider { get; }
        private IOptions<MarvelComicsAPISettings> MarvelAPISettings { get; }

        private static List<IPersonagem> BufferListaPersonagens { get; set; }
        private static List<IComics> BufferListaComics { get; set; }

        #endregion VariaveisDaClasse

        public ServiceAPIMarvel(IServiceProvider serviceProvider, IOptions<MarvelComicsAPISettings> MarvelApiSettings)
        {
            this.ServiceProvider = serviceProvider;
            this.MarvelAPISettings = MarvelApiSettings;

            BufferListaPersonagens = new List<IPersonagem>();
            BufferListaComics = new List<IComics>();
        }

        public List<IPersonagem> ListarTodos()
        {
            if (BufferListaPersonagens.Count == 0)
            {
                var retornoConsulta = ConsultarAPIMarvel(MarvelAPISettings.Value.RecursoCharacters);

                CarregarPersonagemApartirConsulta(retornoConsulta.data.results);
            }

            return BufferListaPersonagens;
        }

        private dynamic ConsultarAPIMarvel(string recursoDesejado)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string ticks = DateTime.Now.Ticks.ToString();
                string hash = GerarHash(ticks);

                HttpResponseMessage response = client.GetAsync(MarvelAPISettings.Value.BaseURL + $"{recursoDesejado}?ts={ticks}&apikey={MarvelAPISettings.Value.PublicKey}&hash={hash}").Result;

                response.EnsureSuccessStatusCode();
                string conteudo = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject(conteudo);
            }
        }

        private string GerarHash(string ticks)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(ticks + MarvelAPISettings.Value.PrivateKey + MarvelAPISettings.Value.PublicKey);
            var gerador = MD5.Create();
            byte[] bytesHash = gerador.ComputeHash(bytes);
            return BitConverter.ToString(bytesHash).ToLower().Replace("-", String.Empty);
        }

        private void CarregarPersonagemApartirConsulta(dynamic DataResults)
        {
            foreach (var item in DataResults)
            {
                IPersonagem personagem = ServiceProvider.GetService<IPersonagem>();
                personagem.IdPersonagem = item.id;
                personagem.Nome = item.name;
                personagem.Descricao = item.description;
                personagem.UrlImagemPersonagem = item.thumbnail.path + "." + item.thumbnail.extension;

                personagem.ListaComics = new List<IComics>();

                personagem.ListaComics = CarregarComicsAPartirConsulta(item.comics.items);

                BufferListaPersonagens.Add(personagem);
            }
        }

        private List<IComics> CarregarComicsAPartirConsulta(dynamic DataResults)
        {
            var ListaComicsAux = new List<IComics>();

            foreach (var item in DataResults)
            {
                IComics comics = ServiceProvider.GetService<IComics>();

                comics.URIRecurso = item.resourceURI;
                comics.Nome = item.name;

                comics.Descricao = item.description ?? String.Empty;

                if (item.thumbnail != null)
                {
                    comics.UrlImagemCapa = (item.thumbnail.path + "." + item.thumbnail.extension);
                }

                if (item.id == null)
                {
                    var ultimoPosicaoBarra = comics.URIRecurso.LastIndexOf("/");
                    comics.IdComic = Convert.ToInt64(comics.URIRecurso.Substring(ultimoPosicaoBarra + 1).ToString());
                }
                else
                {
                    comics.IdComic = Convert.ToInt64(item.id);
                }

                ListaComicsAux.Add(comics);
            }

            return ListaComicsAux;
        }

        public IPersonagem ObterPersonagem(int IdPersonagem)
        {
            IPersonagem retorno = null;

            if (BufferListaPersonagens.Count > 0)
            {
                retorno = BufferListaPersonagens.Where(p => p.IdPersonagem == IdPersonagem).FirstOrDefault();
            }

            if (retorno == null)
            {
                var retornoConsulta = ConsultarAPIMarvel($"{MarvelAPISettings.Value.RecursoCharacters}/{IdPersonagem}");

                CarregarPersonagemApartirConsulta(retornoConsulta.data.results);
                return ObterPersonagem(IdPersonagem);
            }

            return retorno;
        }

        public IComics ObterComics(int IdComics)
        {
            IComics retorno = null;

            if (BufferListaComics.Count > 0)
            {
                retorno = BufferListaComics.Where(c => c.IdComic == IdComics).FirstOrDefault();
            }

            if (retorno == null)
            {
                var retornoConsulta = this.ConsultarAPIMarvel($"{MarvelAPISettings.Value.RecursoComics}/{IdComics}");

                List<IComics> aux = CarregarComicsAPartirConsulta(retornoConsulta.data.results);
                aux.ForEach(a => BufferListaComics.Add(a));

                return ObterComics(IdComics);
            }

            return retorno;
        }
    }
}