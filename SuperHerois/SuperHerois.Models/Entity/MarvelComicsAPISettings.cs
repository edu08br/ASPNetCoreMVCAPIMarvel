﻿using System;

namespace SuperHerois.Models.Entity
{
    public class MarvelComicsAPISettings
    {
        public String BaseURL { get; set; }
        public String PublicKey { get; set; }
        public String PrivateKey { get; set; }
        public String RecursoCharacters { get; set; }
        public String RecursoComics { get; set; }
    }
}