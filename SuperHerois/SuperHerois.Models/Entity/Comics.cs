﻿using SuperHerois.Infraestrutura.Interfaces.Entity;
using System;

namespace SuperHerois.Models.Entity
{
    public class Comics : IComics
    {
        public Int64 IdComic { get; set; }
        public string URIRecurso { get; set; }
        public string Nome { get; set; }
        public String Descricao { get; set; }
        public String UrlImagemCapa { get; set; }
    }
}