﻿using System;
using System.Collections.Generic;
using SuperHerois.Infraestrutura.Interfaces.Entity;

namespace SuperHerois.Models.Entity
{
    public class Personagem : IPersonagem
    {
        public Int64 IdPersonagem { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string UrlImagemPersonagem { get; set; }
        public IList<IComics> ListaComics { get; set; }
    }
}