﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Moq;
using SuperHerois.Infraestrutura.Interfaces.Entity;
using SuperHerois.Infraestrutura.Services;
using SuperHerois.Models.Entity;
using SuperHerois.Models.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace SuperHerois.Testes.Services
{
    public class ServiceAPIMarvelTest
    {
        private IServiceAPIMarvel ServiceApiMarvel { get; set; }

        public ServiceAPIMarvelTest()
        {
            Mock<IServiceProvider> ServiceProviderMock = CriarMock();
            IOptions<MarvelComicsAPISettings> MarvelAPISettings = CriarMarvelAPISettings();

            this.ServiceApiMarvel = new ServiceAPIMarvel(ServiceProviderMock.Object, MarvelAPISettings);
        }

        private Mock<IServiceProvider> CriarMock()
        {
            Mock<IServiceProvider> ServiceProviderMock = new Mock<IServiceProvider>();
            ServiceProviderMock.Setup(x => x.GetService(typeof(IPersonagem))).Returns(new Personagem());
            ServiceProviderMock.Setup(x => x.GetService(typeof(IComics))).Returns(new Comics());
            return ServiceProviderMock;
        }

        private IOptions<MarvelComicsAPISettings> CriarMarvelAPISettings()
        {
            String pastaProjeto = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\.."));
            String PathAppSettings = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\..")) + "\\SuperHerois\\";

            IConfiguration configuracao = new ConfigurationBuilder().AddJsonFile($"{PathAppSettings}appsettings.json", optional: true, reloadOnChange: true).Build();
            var marvelComicsAPISettings = new MarvelComicsAPISettings();
            configuracao.GetSection("MarvelComicsAPI").Bind(marvelComicsAPISettings);

            var services = new ServiceCollection();
            services.Configure<MarvelComicsAPISettings>(configuracao.GetSection("MarvelComicsAPI"));

            return services.BuildServiceProvider().GetRequiredService<IOptions<MarvelComicsAPISettings>>();
        }

        [Fact]
        public void DeveRetornarListaPersonagem()
        {
            List<IPersonagem> resultado = ServiceApiMarvel.ListarTodos();

            Assert.NotEmpty(resultado);
        }

        [Fact]
        public void DeveRetornarPersonagemPelaID()
        {
            //1009220 = Captain America
            IPersonagem resultado = ServiceApiMarvel.ObterPersonagem(1009220);

            Assert.Equal(1009220, resultado.IdPersonagem);
        }

        [Fact]
        public void DeveRetornarComicPelaID()
        {
            //2196 = Exiles(2001) #67
            IComics resultado = ServiceApiMarvel.ObterComics(2196);

            Assert.Equal(2196, resultado.IdComic);
        }
    }
}